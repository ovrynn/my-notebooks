# Imports and other custom stuff
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

# To compare normal distributions based on mean and variance
def compare_normal_dist(data_row):
    title = data_row['Question']
    x_axis = 'Agreement (1 = Strongly Disagree, 5 = Strongly Agree)'
    y_axis = 'Percent Data Amount'
    class_mean = data_row['Class Average']
    dev_mean = data_row['Paper Average']
    class_var = data_row['Class Variance']
    dev_var = data_row['Paper Variance']
    class_std = np.sqrt(class_var)
    dev_std = np.sqrt(dev_var)
    x = np.linspace(1, 5)
    plt.plot(x, stats.norm.pdf(x, class_mean, class_std), label='Class')
    plt.plot(x, stats.norm.pdf(x, dev_mean, dev_std), label='MS Devs')
    plt.title(title)
    plt.xlabel(x_axis)
    plt.ylabel(y_axis)
    plt.legend()
    plt.show()

# Conducts a t-test on summary statistics and outputs the result
def conduct_ttest(data_row):
    class_n = 31 # From the mentimeter results (excluding empty rows)
    dev_n = 564 # From the paper
    class_mean = data_row['Class Average']
    dev_mean = data_row['Paper Average']
    class_var = data_row['Class Variance']
    dev_var = data_row['Paper Variance']
    class_std = np.sqrt(class_var)
    dev_std = np.sqrt(dev_var)
    result = stats.ttest_ind_from_stats(class_mean, class_std, class_n, dev_mean, dev_std, dev_n, equal_var=False)
    print("T-Value: %.6f" % result[0])
    print("P-Value: %.6f" % result[1])